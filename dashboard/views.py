from django.shortcuts import render
from django.urls import reverse_lazy
from django.views.generic import CreateView, ListView, UpdateView, DeleteView, DetailView
from payment.models import Donation


def index(request):
    return render(request, 'dashboard/index.html')


class DonationCreateView(CreateView):
    model = Donation
    template_name = 'dashboard/donation/create.html'
    fields = ["name", "price", "description"]
    success_url = reverse_lazy('index')

    def form_valid(self, form):
        print(form)
        form.instance.user = self.request.user
        print(form.instance.user)
        return super().form_valid(form)

class ProductListView(ListView):
    model = Donation
    template_name = 'dashboard/product/list.jade'
    context_object_name = 'products'
    paginate_by = 13

    def get_context_data(self, **kwargs):
        context = super(ProductListView, self).get_context_data(**kwargs)
        products = self.get_queryset()
        page = self.request.GET.get('page')
        paginator = Paginator(products, self.paginate_by)
        try:
            products = paginator.page(page)
        except PageNotAnInteger:
            products = paginator.page(1)
        except EmptyPage:
            products = paginator.page(paginator.num_pages)
        context['product'] = products
        return context


class ProductUpdateView(UpdateView):
    model = Donation
    template_name = 'dashboard/product/update.jade'
    context_object_name = 'product'
    fields = ('name', 'description', 'price', 'discount', 'categories', 'company')

    def get_success_url(self):
        return reverse_lazy('product-list')


class ProductDeleteView(DeleteView):
    model = Donation
    template_name = 'dashboard/product/delete.html'
    success_url = reverse_lazy('product-list')
