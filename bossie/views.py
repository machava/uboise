from django.http import Http404
from django.shortcuts import render, redirect, HttpResponse
from account.models import  Creator

def index(request):
    creator = Creator.objects.all()

    return render(request, 'kutiva/index.html')



def thanks(request):
    return render(request, 'kutiva/thanks.html')


def price(request):
    return render(request, 'kutiva/price.html')


def security(request):
    return render(request, "about/security.html")


def terms(request):
    return render(request, "about/terms.html")


def policies(request):
    return render(request, "about/policies.html")


def handler404(request, exception):
    return render(request, 'error/404.html', status=404)



def handler500(request):
    return render(request, 'error/500.html', status=500)
