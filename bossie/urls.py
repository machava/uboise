"""kutiva URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""

from django.contrib import admin
from django.urls import path,include
from django.conf.urls.static import static
from . import views
from account.views import *
urlpatterns = [
    path('admin/admin/', admin.site.urls),
    path('creator/<int:pk>', creatorPerfil, name="creator"),
    path('', views.index, name="index"),
    path('thanks', views.thanks, name="thanks"),
    path('account/', include('account.urls')),
    path('dashboard/', include('dashboard.urls')),
    path('payment/', include('payment.urls')),

    path('<str:slug>', creatorPerfilSlug, name="creatorurl"),


]

if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
handler404 = "bossie.views.handler404"
handler500 = "bossie.views.handler500"