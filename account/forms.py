from django import forms
from django.contrib.auth.forms import UserCreationForm
from .models import User, Student, Creator
from django.db import transaction
from django.forms import ModelForm
from tinymce.widgets import TinyMCE

class SignUpForm(UserCreationForm):
    first_name = forms.CharField(max_length=30, required=False, widget=forms.TextInput(attrs={'placeholder': 'Search'}) )
    last_name = forms.CharField(max_length=30, required=False,)
    email = forms.EmailField(max_length=254, help_text='Required. Inform a valid email address.')

    class Meta:
        model = User
        fields = ('first_name', 'last_name', 'email', 'password1', 'password2', )




class StudentSignUpForm(UserCreationForm):
    first_name = forms.CharField(max_length=30, label='Nome',  required=True, widget=forms.TextInput(attrs={'placeholder': 'Nome'}))
    last_name = forms.CharField(max_length=30, label='Apelido', required=True    , widget=forms.TextInput(attrs={'placeholder': 'Apelido'}))
    email = forms.EmailField(max_length=254, label='Email',  widget=forms.TextInput(attrs={'placeholder': 'E-mail'}))
    password1 = forms.CharField(label='Palavra passe',  widget=forms.PasswordInput(attrs={'placeholder': 'Palavra Passe'}))
    password2 = forms.CharField(label='Repitir Palavra Passe ',  widget=forms.PasswordInput(attrs={'placeholder': 'Repitir Palavra Passe  '}))
    class Meta(UserCreationForm.Meta):
        model = User
        fields = ('first_name', 'last_name', 'email', 'password1')
    @transaction.atomic
    def save(self):
        user = super().save(commit=False)

        user.save()

        student = Student.objects.create(user=user)
        student.is_student = False

        return user


class CreatorSignUpForm(UserCreationForm):
    first_name = forms.CharField(max_length=30, label='Nome',  required=True, widget=forms.TextInput(attrs={'placeholder': 'Nome'}))
    last_name = forms.CharField(max_length=30, label='Apelido', required=True    , widget=forms.TextInput(attrs={'placeholder': 'Apelido'}))
    email = forms.EmailField(max_length=254, label='Email',  widget=forms.TextInput(attrs={'placeholder': 'E-mail'}))
    password1 = forms.CharField(label='Palavra passe',  widget=forms.PasswordInput(attrs={'placeholder': 'Palavra Passe'}))
    password2 = forms.CharField(label='Repitir Palavra Passe ',  widget=forms.PasswordInput(attrs={'placeholder': 'Repitir Palavra Passe  '}))
    class Meta(UserCreationForm.Meta):
        model = User
        fields = ('first_name', 'last_name', 'email', 'password1')
    @transaction.atomic
    def save(self):
        user = super().save(commit=False)

        user.save()

        student = Creator.objects.create(user=user)
        student.is_creator = False

        return user





class StudentSignUpdateForm(ModelForm):
    photo = forms.FileField()
    cover = forms.FileField()

    first_name = forms.CharField(label="Nome", widget=forms.TextInput(attrs={'placeholder': 'Nome'}), max_length=30, required=True)
    last_name = forms.CharField(label="Apelido", widget=forms.TextInput(attrs={'placeholder': 'Apelido'}), max_length=30, required=True)
    email = forms.EmailField(label="Email", widget=forms.TextInput(attrs={'placeholder': 'E-mail'}), max_length=254, required=True)
    page_name = forms.CharField(label="Nome da página", widget=forms.TextInput(attrs={'placeholder': 'Nome da página'}),
                               max_length=30, required=False)
    content = forms.CharField(label="O que você está criando?", widget=forms.TextInput(attrs={'placeholder': 'O que você está criando?'}),
                                max_length=30, required=False)

    location = forms.CharField(label="Província", widget=forms.TextInput(attrs={'placeholder': 'Província'}), max_length=30, required=False)
    phone_number = forms.CharField(label="Número de telefone", widget=forms.TextInput(attrs={'placeholder': 'Número de telefone'}), max_length=30)
    description = forms.CharField(label="Descricao sobre a sua página bossie",widget=TinyMCE(attrs={}))
    url = forms.CharField(label="", widget=forms.TextInput(attrs={'placeholder': 'Link da pagina no Boisse'}), max_length=150, required=False)




    class Meta:
        model = Student
        fields = ('photo', 'content', 'page_name', 'location',  'url', 'phone_number', 'description', "photo", 'cover')


