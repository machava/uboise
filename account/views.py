from django.contrib.auth import login, authenticate
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth import logout
from django.urls import reverse_lazy
from django.contrib.auth.decorators import login_required
from django.shortcuts import render, redirect, HttpResponse
from django.contrib.auth.models import User
from django.contrib import messages
from django.views.generic import CreateView, UpdateView, DeleteView, DetailView
import requests

from payment.models import Order, Donation
from .forms import SignUpForm, StudentSignUpForm, StudentSignUpdateForm
from .models import *
from payment.forms import *
import secrets
import json
import requests
def signin(request):
    if request.method == 'POST':
        email = request.POST['username']
        password = request.POST['password']

        user = authenticate(username=email, password=password)

        if user is not None:
            login(request, user)
            return redirect('index')
        else:
            messages.error(request, "E-mail e senha não correspodem.")
    return render(request, 'account/signin.html')


def signup(request):
    if request.method == 'POST':
        form = SignUpForm(request.POST)
        if form.is_valid():
            form.save()
            user.company.commercial_name = commercial_name
            user.company.address = address
            user.company.save()
            email = form.cleaned_data.get('email')
            raw_password = form.cleaned_data.get('password1')
            user = authenticate(username=email, password=raw_password)
            if user is not None:
                login(request, user)
                return redirect('index')

    else:
        form = SignUpForm()
    return render(request, 'account/signup.html', {'form': form})


def sudentsignup(request):
    if request.method == 'POST':
        form = StudentSignUpForm(request.POST)
        if form.is_valid():
            email = form.cleaned_data.get('email')
            raw_password = form.cleaned_data.get('password1')
            phone_number = form.cleaned_data.get('phone_number')
            educational_institution = form.cleaned_data.get('educational_institution')
            user = form.save()
            # user.student.phone_number = phone_number

            # user.student. educational_institution =  educational_institution
            user.student.save()

            user = authenticate(username=email, password=raw_password)
            if user is not None:
                login(request, user)
                return redirect('index')

    else:
        form = StudentSignUpForm()
    return render(request, 'account/student_signup.html', {'form': form})


class   EditPerfile(UpdateView):
    # template_name_suffix = 'account/edit.html'
    template_name = "account/edit.html"
    form_class = StudentSignUpdateForm
    model = Student

    def get_success_url(self, **kwargs):
        return  reverse_lazy('index')

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        return context



class PerfilDetailView(DetailView):
    model = Student
    template_name = 'account/perfil.html'
    context_object_name = 'student'












def creatorPerfilSlug(request, slug):
    creator = Student.objects.get(slug=slug)

    donation = Donation.objects.get(user_id=creator.user.id)


    if request.method == "POST":

        form = PaymentForm(request.POST)
        if form.is_valid():
            order, created = Order.objects.get_or_create(user=creator.user, donation=donation, ordered=False)
            payment = form.save(commit=False)
            payment.name = request.POST.get('name')
            payment.número_de_telefone = request.POST.get('número_de_telefone')
            payment.order = order

            API_ENDPOINT = "https://development-xindiri.herokuapp.com/v1/payments/"
            data = {

                'phone_number': payment.número_de_telefone,
                'amount':  request.POST.get('price'),
                'api_key': 'a0a9fe0bf9178657835ab0ad4b033f9f',

            }

            payment_data = requests.post(url=API_ENDPOINT, data=data)
            print(payment_data)
            response = json.loads(payment_data.text)

            status_code = response["transaction_status_code"]

            if status_code == 201 or status_code == 200:

                payment.save()

                return redirect('thanks')

            else:
                error_message = data['transaction_status']

                messages.error(request, error_message)

                form = PaymentForm()

        #  return redirect('post_detail', pk=post.pk)
    else:
        form = PaymentForm()

    return render(request, 'account/perfil.html', {'creator': creator, 'form': form, 'donation':donation})










def creatorPerfil(request, pk):
    creator = Student.objects.get(pk=pk)
    donation = Donation.objects.get(user_id=pk)


    if request.method == "POST":

        form = PaymentForm(request.POST)
        if form.is_valid():
            order, created = Order.objects.get_or_create(user=creator.user, donation=donation, ordered=False)
            payment = form.save(commit=False)
            payment.name =    request.POST.get('name')

            payment.order = order
            API_ENDPOINT = "https://xpayy.herokuapp.com/payment/"
            data = {

                'contact': payment.número_de_telefone,
                'amount': request.POST.get('price'),
                'reference': secrets.token_hex(6),
                'api_key': '9njrbcqty9ew3cyx4s6k7jvtab134rr6',
                'public_key': '"MIICIjANBgkqhkiG9w0BAQEFAAOCAg8AMIICCgKCAgEAmptSWqV7cGUUJJhUBxsMLonux24u+FoTlrb+4Kgc6092JIszmI1QUoMohaDDXSVueXx6IXwYGsjjWY32HGXj1iQhkALXfObJ4DqXn5h6E8y5/xQYNAyd5bpN5Z8r892B6toGzZQVB7qtebH4apDjmvTi5FGZVjVYxalyyQkj4uQbbRQjgCkubSi45Xl4CGtLqZztsKssWz3mcKncgTnq3DHGYYEYiKq0xIj100LGbnvNz20Sgqmw/cH+Bua4GJsWYLEqf/h/yiMgiBbxFxsnwZl0im5vXDlwKPw+QnO2fscDhxZFAwV06bgG0oEoWm9FnjMsfvwm0rUNYFlZ+TOtCEhmhtFp+Tsx9jPCuOd5h2emGdSKD8A6jtwhNa7oQ8RtLEEqwAn44orENa1ibOkxMiiiFpmmJkwgZPOG/zMCjXIrrhDWTDUOZaPx/lEQoInJoE2i43VN/HTGCCw8dKQAwg0jsEXau5ixD0GUothqvuX3B9taoeoFAIvUPEq35YulprMM7ThdKodSHvhnwKG82dCsodRwY428kg2xM/UjiTENog4B6zzZfPhMxFlOSFX4MnrqkAS+8Jamhy1GgoHkEMrsT5+/ofjCx0HjKbT5NuA2V/lmzgJLl3jIERadLzuTYnKGWxVJcGLkWXlEPYLbiaKzbJb2sYxt+Kt5OxQqC1MCAwEAAQ==',
            }
            # sending post request and saving response as response object
            r = requests.post(url=API_ENDPOINT, data=data)
            pastebin_url = r.text
            print(pastebin_url, 846244186, donation.price)
            payment.save()
            return redirect('thanks')
        #  return redirect('post_detail', pk=post.pk)
    else:
        form = PaymentForm()

    return render(request, 'account/perfil.html', {'creator': creator, 'form': form, 'donation':donation})


@login_required()
def logout_view(request):
    logout(request)
    # Redirect to a succe
